We are an internet marketing company specializing in Local SEO for small and mid-sized businesses. In business for over 9 years, we offer Website Design & Development, Website SEO, Local Business Listing Optimization, AdWords Pay-Per-Click Marketing, Facebook Marketing, and Review Management.

Address: 2084 Fawnwood Dr SE, Grand Rapids, MI 49508, USA

Phone: 616-862-2274

Website: https://on-targetseo.com